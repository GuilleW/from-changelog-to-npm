<div align="center">
<p align="center">
<img src="https://gitlab.com/guillew/from-changelog-to-npm/raw/master/.gitlab/images/readme-header.svg" width="600" height="200" alt="From Changelog to NPM"/>
</p>
<p align="center">
<h3>Write your Changelog and let the watcher create the Release and NPM package automatically!</h3>
</p>
<br/>
</div>


&nbsp;

# From Changelog to NPM

Create tokens in your account, include the watcher in your `.gitlab-ci.yml` and :rocket:.

Just follow the [Keep a Changelog](https://keepachangelog.com/en/1.0.0/) format, and add a new release in your CHANGELOG.md.
The watcher will update the version in your package(-lock).json, create a tag, a release and publish your package to NPM.

<div align="center">
<p align="center">

### Default pipeline

> <img src="https://gitlab.com/guillew/from-changelog-to-npm/raw/master/.gitlab/images/pipeline-default.png" alt="Default pipeline"/>

&nbsp;

### Update [Unreleased] section in CHANGELOG.md

> <img src="https://gitlab.com/guillew/from-changelog-to-npm/raw/master/.gitlab/images/pipeline-canceled.png" alt="Canceled pipeline"/>

&nbsp;

### New Release in CHANGELOG.md

> <img src="https://gitlab.com/guillew/from-changelog-to-npm/raw/master/.gitlab/images/pipeline-release.png" alt="Release pipeline"/>

</p>
</div>


&nbsp;

# Changes

All [details of changes](https://gitlab.com/guillew/from-changelog-to-npm/-/blob/master/CHANGELOG.md) to this project will be documented in this file.


&nbsp;

# Installation & Usage

#### :key: [Create NPM Token](https://www.npmjs.com/settings/~/tokens) to grant publish access:

Click `Create Token`, select `Read and Publish` then click `Create Token` *( :memo: keep the token for the last part)*.

---

#### :key: [Create Gitlab token](https://gitlab.com/profile/personal_access_tokens) to grand git access:

Fill the form with these values:

| Field | Value |
| ----------- | ----------- |
| `Name` | GITLAB_CI_TOKEN |
| `Scopes` > `api` | :ballot_box_with_check: |

then click `Create personal access token` *( :memo: keep the token for the last part)*.

---

#### :abc: Add tokens in Environment variables:

Go to your `Project` > `Settings` > `Continuous Integration/Continuous Delivery`, and expand `Variables` block.

Click `Add variable`

| Field | Value |
| ----------- | ----------- |
| `Key` | NPM_PUBLISH_TOKEN |
| `Value` | :key: [paste your NPM token here] |
| `Mask variable` | :ballot_box_with_check: (mask token in log) |

Click `Add variable`

| Field | Value |
| ----------- | ----------- |
| `Key` | GITLAB_CI_TOKEN |
| `Value` | :key: [paste your gitlab token here] |
| `Mask variable` | :ballot_box_with_check: (mask token in log) |

### Great, your Environment is ready!

The watcher can now access tokens to create Release and publish to NPM for you.

Include the watcher in your `.gitlab-ci.yml` and add the `Release` stage where you want (after your tests, for example):

`.gitlab-ci.yml`
```yaml
include: 'https://gitlab.com/guillew/from-changelog-to-npm/raw/v1.0.0/watch.yml'

stages:
  - Release

```

### That's All Folks!

Now edit your CHANGELOG.md and add a new Release following the [Keep a Changelog](https://keepachangelog.com/en/1.0.0/) format.

:information_source: Pro tips: no need to add date for your release... it's automatic too ! :wink:
` - YYYY-MM-DD` will be added if not set.

```md
## [1.0.0]
### Added
- "From Changelog to NPM" watcher
```


&nbsp;

# Example

`.gitlab-ci.yml`
```yaml
include: 'https://gitlab.com/guillew/from-changelog-to-npm/raw/v1.0.0/watch.yml'

stages:
  - test
  - coverage
  - Release

Test job:
  image: alpine
  script:
    - echo "Hello"

Coverage job:
  stage: coverage
  image: alpine
  script:
    - echo "world !"

```

`CHANGELOG.md`
```md
# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
- You MUST keep the [Unreleased] section to make it work!

## [1.0.0] - 2020-08-16
### Added
- I now use "From Changelog to NPM" pipeline!

```


&nbsp;

# Contribution

Any help or feedback are really welcome, **no matter how great or small**!

Please make sure to read the [Contributing Guide](https://gitlab.com/guillew/from-changelog-to-npm/-/blob/master/CONTRIBUTING.md) before making a pull request.


&nbsp;

# License

[GPL-3.0](https://gitlab.com/guillew/from-changelog-to-npm/-/blob/master/LICENSE)


&nbsp;
