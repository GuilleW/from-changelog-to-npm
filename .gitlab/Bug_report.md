### How to use GitLab

* Please use the 👍 reaction to show that you are affected by the same issue.
* Please don't comment if you have no relevant information to add. It's just extra noise for everyone subscribed to this issue.
* Subscribe to receive notifications on status change and new comments.


### Steps to reproduce
1.
2.
3.

### Expected behaviour
Tell me what should happen

### Actual behaviour
Tell me what happens instead

### Environment
Let us know how you're running `From Changelog to NPM`

<details>
<summary>copy of your .gitlab-ci.yml</summary>

```
Insert your .gitlab-ci.yml here
```
</details>

### Logs

<details>
<summary>CI job log</summary>

```
Insert your CI job log here
```
</details>
