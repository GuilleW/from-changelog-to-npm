# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.0] - 2020-08-16
### Changed
- Improve the README.md
- Release is ready!

## [1.0.0-beta.2] - 2020-08-16
### Fixed
- Pipeline Inception from self repo

## [1.0.0-beta.1] - 2020-08-16
### Added
- Start "From Changelog to NPM" project
